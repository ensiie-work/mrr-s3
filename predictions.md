## TODO
1. Linear model with all columns / features
2. Linear models with some constraint : Ridge & Lasso
3. Test the performance of the models with a test dataset
4. Use Kfold for cross validation

## file : `gas-sensor-array-temperature-modulation/20160930_203718.csv`

### Modèles linéaires simples :

```
Prediction : CO_R8 = [-0.75515895] * R8 + 29.31115670184569
Prediction : CO_R9 = [-0.86057351] * R9 + 28.224689513884776
Prediction : CO_R10 = [-0.59460486] * R10 + 24.890429472233645
Prediction : CO_11 = [-0.63066669] * R11 + 26.97614522669255
Prediction : CO_12 = [-0.73717299] * R12 + 28.20532840958984
Prediction : CO_13 = [-0.73777221] * R13 + 25.941287773346428
Prediction : CO_14 = [-0.56581059] * R14 + 25.72213354930832
```

> Utiliser un autre fichier pour avoir un jeu de test et tester nos modèles pour chaque variable ?

> Rassembler les méthodes pour les 7 détecteurs et avoir un modèle global pour les détecteurs de ce constructeur ?

> Ajouter un fichier csv pour avoir plus de valeurs pour améliorer la précision du modèle ?

> Ajouter le RMSE des modèles

### Ridge avec plusieurs puissances pour R14 :

<img src="ridge.png">

<img src="ridgecoeff.png">

Modèle sélectionné : alpha = 0.01, soit :
$$
CO \eqsim 27 -0.56 \times R14 -0.038 \times R14^2 - 1e-05 \times R14^3 + ...
$$
> Note : les coefficients pour les puissances supérieures ou égales à 4 sont tous inférieurs à 1e-7



<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_CHTML">
</script>
