# Compte rendu n°2
Groupe : Charles ANTEUNIS & Tom VERETOUT

>Suite à notre première analyse, nous avons décidé de nous concentrer sur les résistances du **second constructeur**. 
>
>Egalement,  afin de permettre une analyse des données, nous avons décidé de **regrouper** les moyennes de résistances en fonction d'une **même concentration**. Ceci nous permet d'obtenir un jeu de données utilisable pour l'analyse. Leur étude simple nous permet de conforter notre choix de nous porter uniquement sur les résistance du second constructeur.


## Corrélation

En affichant le graphique des correlations (`corrplot`), on se rend compte que le comportement observé lors dans le premier fichier n'est pas une exeption et est redondant dans les autres. Ceci conforte notre volonté de nous interesser qu'aux capteurs R8 à R14.

<img src="./corr1.png" width="200"> <img src="./corr2.png" width="200"> <img src="./corr3.png" width="200">


## Création de modèles linéaires simples : 

Pour les 7 dernières resistances, on cherches à établir les modèles linéaires simples. On affiche ici quelques résultats :


```
Prediction : CO_R8 = [-0.75515895] * R8 + 29.31115670184569
Prediction : CO_R9 = [-0.86057351] * R9 + 28.224689513884776
Prediction : CO_12 = [-0.73717299] * R12 + 28.20532840958984
Prediction : CO_14 = [-0.56581059] * R14 + 25.7221
```
On remarque que les models sont similaires pour toutes les résistances et suivent grossièrement nos différentes données. 

Cherchons maintenant à améliorer ce modèle avec les méthodes de RIDGE et LASSO.


## Ridge avec plusieurs puissances pour R14 :
<center>
<img src="ridge.png" width="400">
</center>
<img src="ridgecoeff.png">

Modèle sélectionné : alpha = 0.01, soit :
$$
CO \eqsim 27 -0.56 \times R14 -0.038 \times R14^2 - 1e-05 \times R14^3 + ...
$$

Notre choix s'est porté sur 

> Note : les coefficients pour les puissances supérieures ou égales à 4 sont tous inférieurs à 1e-7. On pourrait donc considèrer un modèle approché par un polynome de degrès 3.

## Etude de la pertinence du model

* On détermine le RMSE pour le model établi. Pour cela on détermine le model pour un jeu de données d'entrainement et on calcule le RMSE pour un autre jeu de donné de test.
On obtient les résultats suivants :
    * Pour le jeu d'entrainement \$RMSE = 0.48$\$

* En déterminant le RSS





<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_CHTML">
</script>
