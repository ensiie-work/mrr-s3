import csv
import matplotlib.pyplot as plt
import statistics
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import mean_squared_error
from math import sqrt
import numpy as np
import pandas as pd

data = []

def ridge_regression(data, predictors, alpha, models_to_plot={}):
    #Fit the model
    ridgereg = Ridge(alpha=alpha,normalize=True)
    ridgereg.fit(data[predictors],data['y'])
    y_pred = ridgereg.predict(data[predictors])
    
    #Check if a plot is to be made for the entered alpha
    if alpha in models_to_plot:
        plt.subplot(models_to_plot[alpha])
        plt.tight_layout()
        plt.plot(data['x'],y_pred)
        plt.plot(data['x'],data['y'],'.')
        print('For alpha: %.3g, RMSE = %d'%(alpha,sqrt(mean_squared_error(data['y'], y_pred))) )
        plt.title('Plot for alpha: %.3g'%alpha)
    
    #Return the result in pre-defined format
    rss = sum((y_pred-data['y'])**2)
    ret = [rss]
    ret.extend([ridgereg.intercept_])
    ret.extend(ridgereg.coef_)
    return ret

def lasso_regression(data, predictors, alpha, models_to_plot={}):
    #Fit the model
    lassoreg = Lasso(alpha=alpha,normalize=True, max_iter=1e5)
    lassoreg.fit(data[predictors],data['y'])
    y_pred = lassoreg.predict(data[predictors])
    
    #Check if a plot is to be made for the entered alpha
    if alpha in models_to_plot:
        plt.subplot(models_to_plot[alpha])
        plt.tight_layout()
        plt.plot(data['x'],y_pred)
        plt.plot(data['x'],data['y'],'.')
        plt.title('Plot for alpha: %.3g'%alpha)
    
    #Return the result in pre-defined format
    rss = sum((y_pred-data['y'])**2)
    ret = [rss]
    ret.extend([lassoreg.intercept_])
    ret.extend(lassoreg.coef_)
    return ret

with open ("gas-sensor-array-temperature-modulation/20160930_203718.csv") as data:
    spamreader = csv.DictReader(data, delimiter=',')
    R1 = []
    CO = []
    for row in spamreader:
        R1.append(float(row['R14 (MOhm)']))
        CO.append(float(row['CO (ppm)']))


    n = len(R1)
    split = n//10
    res = [[],[],[],[],[],[],[],[],[],[],]
    for i in range(n):
        ind = i//split
        if ind == 10:
            break
        #print("ind: {0}, i: {1}, split: {2}".format(ind, i, split))
        if abs(CO[i] - 0.0) < 0.01:
            res[0].append(R1[i])
        elif abs(CO[i] - 2.220) < 0.01:
            res[1].append(R1[i])
        elif abs(CO[i] - 4.440) < 0.01:
            res[2].append(R1[i])
        elif abs(CO[i] - 6.670) < 0.01:
            res[3].append(R1[i])
        elif abs(CO[i] - 8.890) < 0.01:
            res[4].append(R1[i])
        elif abs(CO[i] - 11.110) < 0.01:
            res[5].append(R1[i])
        elif abs(CO[i] - 13.330) < 0.01:
            res[6].append(R1[i])
        elif abs(CO[i] - 15.56) < 0.01:
            res[7].append(R1[i])
        elif abs(CO[i] - 17.78) < 0.01:
            res[8].append(R1[i])
        elif abs(CO[i] - 20.0) < 0.01:
            res[9].append(R1[i])

    COsample = [20/9*i for i in range(10)]
    R1mean = []
    for r in res:
        R1mean.append(statistics.mean(r))

    data = pd.DataFrame(np.column_stack([R1mean,COsample]),columns=['x','y'])

for i in range(2,16):  #power of 1 is already there
    colname = 'x_%d'%i      #new var will be x_power
    data[colname] = data['x']**i

#Initialize predictors to be set of 15 powers of x
predictors=['x']
predictors.extend(['x_%d'%i for i in range(2,16)])

#Set the different values of alpha to be tested
alpha_ridge = [1e-15, 1e-10, 1e-8, 1e-4, 1e-3,1e-2, 1e-1, 1, 5, 10]

#Initialize the dataframe for storing coefficients.
col = ['rss','intercept'] + ['coef_x_%d'%i for i in range(1,16)]
ind = ['alpha_%.2g'%alpha_ridge[i] for i in range(0,10)]
coef_matrix_ridge = pd.DataFrame(index=ind, columns=col)

models_to_plot = {1e-8:231, 1e-3:232, 1e-2:233, 1e-1:234, 1:235, 5:236}
for i in range(10):
    coef_matrix_ridge.iloc[i,] = ridge_regression(data, predictors, alpha_ridge[i], models_to_plot)

"""
x = np.array(R1mean).reshape((-1,1))
y = np.array(COsample)

model = LinearRegression().fit(x, y)
y_pred = model.predict(x)
print("Prediction : CO = {0} * R14 + {1}".format(model.coef_, model.intercept_))
plt.scatter(R1mean, COsample)
plt.plot(R1mean, y_pred)
plt.show()
"""
pd.options.display.float_format = '{:,.2g}'.format
print(coef_matrix_ridge)
plt.show()